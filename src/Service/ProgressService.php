<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\GoalType;
use App\Entity\GroupInfo;
use App\Entity\Progress;
use App\Repository\GoalRepository;
use App\Repository\GroupInfoRepository;
use App\Repository\ProgressRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

readonly class ProgressService
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private GoalRepository $goalRepository,
        private GroupInfoRepository $groupRepository,
        private ProgressRepository $progressRepository,
    ) {
    }

    public function oldRead(): array
    {
        $data = [[]];

        $totals = new Progress('Batch 0 total');
        $totals->setGroupInfo(
            $this->entityManager->getReference(GroupInfo::class, 1)
        );

        foreach ($this->progressRepository->findBy(['groupInfo' => 1], ['itemOrder' => 'ASC']) as $progress) {
            $totals->addProgress($progress);
            $data[$progress->getId()] = $progress->toArray();
        }

        $data[0] = $totals->toArray();

        return $data;
    }

    public function readGroup(): array
    {
        $data = [];

        foreach ($this->groupRepository->findAll() as $group) {
            $groupId = $group->getShortName();

            $groupTotals = new Progress('Total ' . $group->getShortName());
            $groupTotals->setGroupInfo($group);

            $data[$groupId] = [
                'title' => $group->getName(),
                'data' => [],
            ];

            foreach ($this->progressRepository->findBy(['groupInfo' => $group], ['itemOrder' => 'ASC']) as $progress) {
                if (!$progress->isStandalone()) {
                    $groupTotals->addProgress($progress);
                }
                $data[$groupId]['data'][] = $progress->toArray();
            }

            $groupTotals->setTotal($group->getTotal());

            $data[$groupId]['data'] = array_merge(
                [$groupTotals],
                $this->getGoalStats(GoalType::ANNUAL, $group, $groupTotals),
                $this->getGoalStats(GoalType::QUARTILE, $group, $groupTotals),
                $data[$groupId]['data'],
            );
        }

        foreach ($data as &$datum) {
            $datum['data'][0] = $datum['data'][0]->toArray();
        }
        unset ($datum);

        return $data;
    }

    public function create(InputBag $request): Progress
    {
        if (!$request->has('name') || !$request->has('groupInfo')) {
            throw new BadRequestHttpException('Parameters "name" and "groupInfo" are required');
        }

        $progress = new Progress(mb_substr($request->get('name'), 0, 40, 'UTF-8'));
        $progress->setGroupInfo(
            $this->entityManager->getReference(GroupInfo::class, (int) $request->get('groupInfo'))
        );

        if ($request->has('total') && is_int($request->get('total'))) {
            $progress->setTotal((int) $request->get('total'));
        }

        $this->entityManager->persist($progress);
        $this->entityManager->flush();

        return $progress;
    }

    public function update(Progress $progress, InputBag $request): void
    {
        $allowedToUpdate = ['name', 'done', 'total', 'collapse'];

        foreach ($allowedToUpdate as $property) {
            if ($request->has($property)) {
                $method = match ($property) {
                    'name' => $this->updateName(...),
                    'done' => $this->updateDone(...),
                    'total' => $this->updateTotal(...),
                    'collapse' => $this->updateCollapse(...),
                };

                $method($progress, $request->get($property));
            }
        }

        $this->entityManager->flush();
    }

    public function delete(Progress $progress): void
    {
        if ($progress->getDone() !== 0) {
            throw new BadRequestHttpException('Cannot delete progressbar with non-zero done amount');
        }

        $this->entityManager->remove($progress);
        $this->entityManager->flush();
    }

    private function updateName(Progress $progress, int|string $value): void
    {
        $progress->setName(mb_substr($value, 0, 40, 'UTF-8'));
    }

    private function updateDone(Progress $progress, int|string $value): void
    {
        if (str_starts_with($value, '+')) {
            $progress->addDone((int) substr($value, 1));
        } else {
            $progress->setDone((int) $value);
        }

        if ($progress->getDone() > $progress->getTotal()) {
            $progress->setTotal($progress->getDone());
        }
    }

    private function updateTotal(Progress $progress, int|string $value): void
    {
        $total = (int) $value;

        if ($total < $progress->getDone()) {
            throw new BadRequestHttpException('Total cannot be lower than done');
        }

        $progress->setTotal($total);
    }

    private function updateCollapse(Progress $progress, int|string $value): void
    {
        $collapse = (int) $value;

        if ($collapse < 0 || $collapse > 1) {
            throw new BadRequestHttpException('Value out of bounds 0..1');
        }

        $progress->setCollapse($collapse);
    }


    private function getGoalStats(GoalType $type, GroupInfo $group, Progress $groupTotals): array
    {
        $finishedGoalsSum = 0;
        $data = [];

        $activeGoalProgress = null;
        foreach ($this->goalRepository->findBy(['type' => $type, 'groupInfo' => $group], ['name' => 'DESC']) as $goal) {
            $goalProgress = $goal->getProgress();
            $finishedGoalsSum += $goal->getFinishedAmount();

            if ($goal->getFinishedAmount() === 0) {
                $activeGoalProgress = $goalProgress;
            } else {
                $data[] = $goalProgress->toArray();
            }
        }

        if ($activeGoalProgress !== null) {
            $activeGoalProgress->setDone($groupTotals->getDone() - $finishedGoalsSum);
            array_unshift($data, $activeGoalProgress->toArray());
        }

        return $data;
    }
}
