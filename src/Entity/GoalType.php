<?php

declare(strict_types=1);

namespace App\Entity;

enum GoalType: string
{
    case ANNUAL = 'annual';
    case QUARTILE = 'quartile';
}
