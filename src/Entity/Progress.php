<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\ProgressRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProgressRepository::class)]
#[ORM\Index(columns: ['item_order'], name: 'idx_order')]
class Progress
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    private string $name;

    #[ORM\Column(type: 'integer')]
    private int $done;

    #[ORM\Column(type: 'integer')]
    private int $total;

    #[ORM\Column(type: 'integer')]
    private int $itemOrder;

    #[ORM\ManyToOne(targetEntity: GroupInfo::class)]
    private ?GroupInfo $groupInfo;

    #[ORM\Column(type: 'integer')]
    private int $collapse;

    #[ORM\Column(type: 'boolean')]
    private bool $standalone;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->done = 0;
        $this->total = 0;
        $this->groupInfo = null;
        $this->itemOrder = 0;
        $this->collapse = 0;
        $this->standalone = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDone(): ?int
    {
        return $this->done;
    }

    public function setDone(int $done): self
    {
        $this->done = $done;

        return $this;
    }

    public function addDone(int $diff): self
    {
        $this->done += $diff;

        return $this;
    }

    public function getTotal(): ?int
    {
        return $this->total;
    }

    public function setTotal(int $total): void
    {
        $this->total = $total;
    }

    public function getItemOrder(): int
    {
        return $this->itemOrder;
    }


    public function getGroupInfo(): ?GroupInfo
    {
        return $this->groupInfo;
    }

    public function setGroupInfo(?GroupInfo $groupInfo): self
    {
        $this->groupInfo = $groupInfo;

        return $this;
    }

    public function getGroupName(): ?string
    {
        return $this->groupInfo?->getShortName();
    }

    public function getCollapse(): int
    {
        return $this->collapse;
    }

    public function setCollapse(int $collapse): self
    {
        $this->collapse = $collapse;

        return $this;
    }

    public function isStandalone(): bool
    {
        return $this->standalone;
    }

    public function addProgress(Progress $progress): self
    {
        $this->done += $progress->done;
        $this->total += $progress->total;

        return $this;
    }

    public function getPercent(): string
    {
        if ($this->total === 0) {
            return '0';
        }

        return sprintf('%0.1f', (min($this->done, $this->total) / $this->total) * 100);
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'done' => $this->done,
            'total' => $this->total,
            'percent' => $this->getPercent(),
            'groupInfo' => $this->getGroupInfo()->getId(),
            'collapse' => $this->collapse,
        ];
    }
}
