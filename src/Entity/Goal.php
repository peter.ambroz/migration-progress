<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\GoalRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GoalRepository::class)]
class Goal
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    private string $name;

    #[ORM\Column(type: 'string', enumType: GoalType::class)]
    private GoalType $type;

    #[ORM\Column(type: 'integer')]
    private int $amount;

    #[ORM\Column(type: 'integer')]
    private int $finishedAmount;

    #[ORM\ManyToOne(targetEntity: GroupInfo::class)]
    private GroupInfo $groupInfo;

    #[ORM\Column(type: 'integer')]
    private int $collapse;

    public function __construct()
    {
        $this->collapse = 0;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getType(): GoalType
    {
        return $this->type;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getFinishedAmount(): int
    {
        return $this->finishedAmount;
    }

    public function getProgress(): Progress
    {
        $progress = new Progress($this->getName());
        $progress->setDone($this->finishedAmount);
        $progress->setTotal($this->amount);
        $progress->setCollapse($this->collapse);
        $progress->setGroupInfo($this->groupInfo);

        return $progress;
    }

    public function getCollapse(): int
    {
        return $this->collapse;
    }
}
