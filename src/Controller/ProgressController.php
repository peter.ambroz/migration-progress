<?php

namespace App\Controller;

use App\Entity\Progress;
use App\Service\ProgressService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/progress')]
class ProgressController extends AbstractController
{
    #[Route('/', name: 'list', methods: ['GET'])]
    public function index(ProgressService $progressService): Response
    {
        return $this->json($progressService->oldRead());
    }

    #[Route('/', name: 'create', methods: ['POST'])]
    public function addProgressbar(Request $request, ProgressService $progressService): Response
    {
        $progress = $progressService->create($request->request);

        return $this->json($progress->toArray(), Response::HTTP_CREATED);
    }

    #[Route('/group', name: 'group', methods: ['GET'])]
    public function groupRead(ProgressService $progressService): Response
    {
        return $this->json($progressService->readGroup());
    }

    #[Route('/{progress}', name: 'read', requirements: ['progress' => '\d+'], methods: ['GET'])]
    public function read(Progress $progress): Response
    {
        return $this->json($progress->toArray());
    }

    #[Route('/{progress}', name: 'update', requirements: ['progress' => '\d+'], methods: ['POST', 'PUT'])]
    public function update(Progress $progress, Request $request, ProgressService $progressService): Response
    {
        $progressService->update($progress, $request->request);

        return $this->json($progress->toArray());
    }

    #[Route('/{progress}', name: 'delete', requirements: ['progress' => '\d+'], methods: ['DELETE'])]
    public function delete(Progress $progress, ProgressService $progressService): Response
    {
        $progressService->delete($progress);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
