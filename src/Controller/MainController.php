<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    #[Route('/', name: 'main')]
    public function index(Request $request): Response
    {
        if ($request->getHttpHost() === 'migration.comp.se') {
            return new RedirectResponse('https://odinmigrate.it', Response::HTTP_MOVED_PERMANENTLY);
        }

        return new Response(file_get_contents('index.html'));
    }
}
