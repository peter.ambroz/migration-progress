<?php

namespace App\Repository;

use App\Entity\GroupInfo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GroupInfo|null find($id, $lockMode = null, $lockVersion = null)
 * @method GroupInfo|null findOneBy(array $criteria, array $orderBy = null)
 * @method GroupInfo[]    findAll()
 * @method GroupInfo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GroupInfoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GroupInfo::class);
    }
}
