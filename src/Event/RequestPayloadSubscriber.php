<?php

declare(strict_types=1);

namespace App\Event;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class RequestPayloadSubscriber implements EventSubscriberInterface
{
    public function unpackPayload(RequestEvent $requestEvent)
    {
        if (!$requestEvent->isMainRequest()) {
            return;
        }

        $request = $requestEvent->getRequest();

        if ($request->getContentTypeFormat() === 'json' && $request->getContent() !== '') {
            $request->request->replace($request->toArray());
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => ['unpackPayload', 12],
        ];
    }
}
